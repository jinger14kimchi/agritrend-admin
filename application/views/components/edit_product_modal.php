 
<!-- Modal Edit  -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Details</h4>
      </div>
        <form action="<?php echo base_url('/pages/commodities/updateproduct'); ?>" method="POST">
            <div class="modal-body">      
                <div class="row">   
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Product ID</label><sup class="required-field"></sup>
                            <input type="text" class="form-control" id="edit-prod-id" name="product_id">
                        </div>
                    </div>
                </div>    
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Category</label><sup class="required-field"> *field required</sup><br>
                                <select class="btn-group select" name="category_id" id="edit-category" required>
                                 <?php foreach ($categories as $cat) { ?>
                                    <option value="<?php echo $cat->category_id ?>"><?php echo $cat->category_name ?></option>
                               <?php  } ?>
                                </select>
                        </div>
                    </div>
                </div>          
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Name</label><sup class="required-field"> *field required</sup>
                            <input type="text" class="form-control" id="edit-prod-name" name="product_name" placeholder="Name" required>
                        </div>
                    </div>
                </div>              
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" class="form-control" id="edit-desc" name="product_description" placeholder="Name" required>
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div> 
            <div class="clearfix"></div>
        </form>
        </div>
      </div>
    </div>

<!-- End Modal Edit -->
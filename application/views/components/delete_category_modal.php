

<!-- Modal Delete  -->

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete</h4>
      </div>
        <form action="<?php echo base_url('/pages/commodities/delete'); ?>" method="POST">
            <div class="modal-body center-text">
                <p class="center-text">Are you sure you want to delete this item?</p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-danger">Confirm Delete</button>
            </div> 
            <div class="clearfix"></div>
        </form>
        </div>
      </div>
    </div>

<!-- End Modal Delete -->


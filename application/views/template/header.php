
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <title>AgriTrend</title>

    <link rel='icon' href='<?= base_url('template/assets/images/favicon.ico') ?>' type='image/x-icon' />
    <link rel="stylesheet" type="text/css" href="<?= base_url('template/assets/css/bootstrap.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('template/assets/css/animate.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('template/assets/css/light-bootstrap-dashboard.css?v=1.4.0'); ?>">
    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chartist/0.11.0/chartist.min.css" />
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="<?= base_url('template/assets/css/pe-icon-7-stroke.css'); ?>" rel="stylesheet" />
    <link href="<?= base_url('template/assets/css/style.css'); ?>" rel="stylesheet" />

</head>
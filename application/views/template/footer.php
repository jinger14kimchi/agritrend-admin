
        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="#">
                                Home
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Company
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Portfolio
                            </a>
                        </li>
                        <li>
                            <a href="#">
                               Blog
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                </p>
            </div>
        </footer>

				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
				<script src="<?= base_url('template/assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>

				<!--  Charts Plugin -->
				<script src="<?= base_url('template/assets/js/chartist.min.js'); ?>"></script>

				<!--  Notifications Plugin    -->
				<script src="<?= base_url('template/assets/js/bootstrap-notify.js'); ?>"></script>

				<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
				<script src="<?= base_url('template/assets/js/light-bootstrap-dashboard.js?v=1.4.0'); ?>"></script>


                <script type="text/javascript">
        $(document).ready(function() {
            $(".nav li ").on("click", function() {
                $(" .nav li").removeClass("active");
                $(this).addClass("active");
            });
        });
    </script>
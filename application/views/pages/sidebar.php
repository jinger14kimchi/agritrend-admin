<style type="text/css">
    li{list-style-type: none;}

    .submenu{ margin-left: 0 !important; text-align: center}
    .a:hover{color: white!important;}
    .dropdown-menu{right:15px; border-radius: 5px; margin-left: 15px; top:49px;    background-color: #51ba51;
    border: none;
    border-top-right-radius: 0;
    border-top-left-radius: 0;
 }

</style>

 
    <div class="sidebar" data-color="green">
      <div class="sidebar-wrapper">
            <div class="logo">
                <a href="" class="simple-text">
                    AgriTrend
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="<?= base_url('pages/dashboard'); ?>"  >
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="<?= base_url('pages/commodities'); ?>"  >
                        <i class="pe-7s-user"></i>
                        <p>Commodities</p>
                    </a>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <i class="pe-7s-note2"></i>
                        <p>Price Reports<b class="caret"></b></p>
                    </a>
                    <ul class="dropdown-menu">                            
                        <li>
                            <a href="<?= base_url('pages/price/reports'); ?>" class="a">
                                <p class="submenu">Monitoring</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <i class="pe-7s-news-paper"></i>
                        <p>Accounts<b class="caret"></b></p>
                    </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?= base_url('pages/accounts/agents'); ?>"  class="a">
                                    <p class="submenu">Agents</p>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('pages/accounts/users'); ?>" class="a">
                                    <p class="submenu">Users</p>
                                </a>
                            </li>
                            <li>
                                <a href="<?= base_url('pages/accounts/admin'); ?>" class="a">
                                    <p class="submenu">Admin</p>
                                </a>
                            </li>
                        </ul>
                </li>
                <li>
                    <a href="<?= base_url('pages/trend'); ?>">
                        <i class="pe-7s-science"></i>
                        <p>Trend Analysis</p>
                    </a>
                </li>

                <li class="">
                    <a href="<?= base_url('pages/settings'); ?>">
                        <i class="pe-7s-bell"></i>
                        <p>Settings</p>
                    </a>
                </li>
            </nav>    
        </ul>
      </div>
    </div>


<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('template/header');
 ?>
<body>

<div class="wrapper">
    <?php $this->load->view('pages/sidebar'); ?>

    <div class="main-panel">
        <?php $this->load->view('pages/navigation'); ?>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">List of <?php echo $type; ?> Accounts</h4>   

                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Name </th>
                                    	<th>Username</th>
                                        <th>Birthdate</th>
                                        <th>Gender</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($users as $user) { ?>   
                                        <tr id="<?php echo $user->user_id ?>">                                     
                                            <td class="user_id"><?php echo $user->user_id ?></td>                                       
                                            <td class="user_fullname"><?php echo $user->fname . ' ' . $user->lname?></td>                                 
                                            <td class="username"><?php echo $user->username ?></td>                                    
                                            <td class="birthdate"><?php echo $user->birthdate ?></td>                              
                                            <td class="gender"><?php echo $user->gender ?></td>                                       
                                            <td class="date_created"><?php echo $user->date_created ?></td>                                   
                                            <td>
                                              <i class="pe-7s-pen icons"></i>&nbsp;
                                              <span  id="<?php echo $user->user_id ?>" data-whatever="<?php echo $user->user_id ?>" data-toggle="modal" data-target="#editUserModal">Edit</span> &nbsp;
                                         	</td>
                                        </tr>
                                       <?php } ?>
                                       
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
	<?php $this->load->view('template/footer');  ?>


    </div>
</div>


<!-- Modal Edit  -->

<div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Details</h4>
      </div>
        <form action="<?php echo base_url('/pages/users/updateuser'); ?>" method="POST">
            <div class="modal-body">      
                <div class="row">   
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>User ID</label><sup class="required-field"></sup>
                            <input type="text" class="form-control" id="edit-user-id" name="user_id">
                        </div>
                    </div>  
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control" id="edit-username" placeholder="Username" name="username">
                        </div>
                    </div>
                </div>            
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" id="edit-fname" name="fname" placeholder="First Name" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" id="edit-lname" name="lname" placeholder="Last Name" required>
                        </div>
                    </div>
                </div>       
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Birthdate</label>
                            <input type="text" class="form-control" id="edit-birthdate" name="birthdate" placeholder="Birthdate" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Gender</label><br>
                                <select class="btn-group select" id="gender" name="gender" required>
                                    <option value="female">Female</option>
                                    <option value="male">Male</option>
                              
                                </select>
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div> 
            <div class="clearfix"></div>
        </form>
        </div>
      </div>
    </div>

<!-- End Modal Edit -->


<script type="text/javascript">
   $('#editUserModal').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget) 
        let recipient = button.data('whatever') 
        let id = recipient;
        $.ajax({    
            type: "GET",
            url: "http://localhost/agritrend-web/api/users/users",
            data: {id:id},
            dataType: "json",               
            success: function(res){  
                let data = res[0];
                console.log(data);
                $('#edit-user-id').val(data.user_id);
                $('#edit-username').val(data.username);
                $('#edit-fname').val(data.fname);
                $('#edit-lname').val(data.lname);
                $('#edit-birthdate').val(data.birthdate);
                console.log(data.gender);
                $('select#gender option[value='+data.gender+']').attr("selected",true);            
            }
        });
    })
</script>

</body>
</html>
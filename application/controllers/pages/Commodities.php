<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

class Commodities extends CI_Controller {

    function __construct() {
        parent::__construct(); 
		$this->load->model('CommodityModel', 'commoditymodel');
	}
	
	function index() {
		$commodities = json_decode(
			file_get_contents(base_url('/api/product/product'))
		);
		$data['commodities'] = $commodities;
		$data['categories'] = $this->getCategories();
		if($this->check_access()){    
				$this->load->view('pages/commodities', $data);
		}	
	}

	public function getCommodities() {
		$commodities = json_decode(
			file_get_contents(base_url('/api/product/product'))
		);
		$data['commodities'] = $commodities;
		$data['categories'] = $this->getCategories();
		if($this->check_access()){    
				$this->load->view('pages/commodities', $data);
		}	
}

	public function getCategories() {		
		$categories = json_decode(
			file_get_contents(base_url('/api/category/category'))
		);
        return $categories;
	}

	public function addNew() {
		$this->load->model('ProductModel', 'productmodel');
		$data = $this->input->post();
		$id = $this->productmodel->addNewProduct($data);
		if ($id) {
			redirect(base_url('pages/commodities'));
		}
	}

	public function updateProduct() {
		$this->load->model('ProductModel', 'productmodel');
		$data = $this->input->post();
		$affected = $this->productmodel->updateProduct($data);
		if ($affected > 0) {
		$data['msg'] = "Successfully Updated Product!";
            $this->load->view('pages/commodities', $data);
		}
	}

	public function addCategory() {
		$this->load->model('CategoryModel', 'categorymodel');
		$data = $this->input->post();
		$affected = $this->categorymodel->addNewCategory($data);
		if ($affected > 0) {
			$data['msg'] = "Successfully Added New Category!";
            $this->load->view('pages/commodities', $data);
		}
	}

	public function check_access(){
		if ($this->session->userdata('validated') == true && $this->session->userdata('user_type') == 'admin'){
			return true;
		} 
		else{
			$data['msg'] = "Access denied";
            $this->load->view('pages/loginpage', $data);

		}
	}
}

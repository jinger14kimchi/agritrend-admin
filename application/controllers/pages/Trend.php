<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trend extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model('UsersModel', 'usersmodel');
		$this->load->model('TrendModel', 'trendmodel');
		$this->load->model('CategoryModel', 'categorymodel');
    }

	function index() {

	if($this->check_access()){
		$this->getTrend();
		// $this->load->view('pages/trend');
	}
	}




	public function check_access(){
			if($this->session->userdata('validated') == true && $this->session->userdata('user_type') == 'admin'){
				return true;
			}else{
				$data['msg'] = "Access denied";
	            $this->load->view('pages/loginpage', $data);
			}
	}

	public function getTrend() {
		$data['result'] = $this->trendmodel->getTrend();
		$data['products'] = $this->trendmodel->getProduct();
		$data['categories'] = $this->categorymodel->getCategories();
		$this->load->view('pages/trend', $data);


	}	

}


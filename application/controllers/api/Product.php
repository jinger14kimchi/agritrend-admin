<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Product extends REST_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->model('ProductModel', 'productmodel');
    }

    public function product_get() {
        $id = $this->get('id');
        $market_id = $this->get('market_id');
        $category_id = $this->get('category_id');

        if ($id === NULL && $market_id === NULL && $category_id === NULL) {
            $products = $this->productmodel->getProducts();
            if ($products) {
                $this->response($products, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
        }        
        else if ($market_id !== NULL) {

            $productsByMarket = $this->productmodel->getProductByMarket($market_id);
        }

        else {        	
            $product = $this->productmodel->getProduct($id);
            $this->response($product, 200);
        }
    }

    public function units_get() {
        $units = $this->productmodel->getProductUnit();
        if ($units) {
            $this->response($units, 200);
        }
        else {
            $this->response(['status' => false], 404);
        }
    }

    public function product_post() {
        $product_info = $this->post();
        $id = $this->productmodel->addNewProduct($product_info);
        if ($id !== NULL) {
            $this->response([
                'id' => $id,
                'status' => TRUE,
                'message' => 'Successfully added product'
            ], REST_Controller::HTTP_OK);
        }
    }

    public function category_get() {
        $id = $this->get('id');
        $product = $this->productmodel->getProductByCategory($id);
        if ($product) {
            $this->response($product, 200);
        } else {
            $this->response("Not found", 404); 
        }

    }

}
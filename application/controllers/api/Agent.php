<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Agent extends REST_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->model('AgentModel', 'agentmodel');
        $this->load->model('AddressModel', 'addressmodel');
        $this->load->model('UsersModel', 'usersmodel');
    }


    public function agent_get() {
        // if mag padala kag parameter na id ang name, iyang pagsabot gusto nimo kwaon ang info anang
        // agent with the id number
        // if wala kay gipadala na params, iyang pagsabot kay tanan agents imong gusto makita
        $id = $this->get('id');

        if ($id === NULL) {
            $agents = $this->agentmodel->getAgents();
            if ($agents) {
                $this->response($agents, REST_Controller::HTTP_OK);
            }

        }
        else {        	
            $agent = $this->agentmodel->getAgent($id);
            $this->response($agent, 200);
        }
    }

    public function agent_post() {        
        $id = $this->post('id');
        
        // if id is NULL, meaning mag add ug new agent 
        if ($id === NULL) {                
            $address = array(
                'street' => $this->post('street'),
                'barangay' => $this->post('barangay'),
                'city' => $this->post('city'),
                'zipcode' => $this->post('street')
            );
            $address_id = $this->addressmodel->insertAddress($address);

            if ($address_id) {                
                $info = array(
                    'fname' => $this->post('fname'),
                    'lname' => $this->post('lname'),
                    'birthdate' => $this->post('birthdate'),
                    'gender' => $this->post('gender'),
                    'username' => $this->post('username'),
                    'password' => $this->post('password'),
                    'user_type' => $this->post('user_type'),
                    'address_id' => $address_id
                );
                $last_id = $this->usersmodel->addUser($info);

                if ($last_id) {                    
                    $this->response([
                        'status' => TRUE,
                        'message' => 'Successfully added user.'
                    ], REST_Controller::HTTP_OK);
                }
                else {
                    return 'Please check you information and try again';
                }
            }
            else {
                return "There's an error in the address";
            }
        }
        else {
            // else, naay id gipadala, meaning iupdate ni na agent
            $user = $this->usersmodel->getUser($id);

            $this->response($user, 200);
        }
    }
    }
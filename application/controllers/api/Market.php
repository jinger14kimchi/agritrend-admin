<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Market extends REST_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->model('MarketModel', 'marketmodel');
        $this->load->model('AddressModel', 'addressmodel');
    }

    public function market_get() {
        $id = $this->get('id');

        if ($id === NULL) {
            $markets = $this->marketmodel->getMarkets();
            if ($markets) {
                $this->response($markets, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }

        }
        else {        	
            $market = $this->marketmodel->getMarket($id);
            $this->response($market, 200);
        }
    }

}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ReportingModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

	public function getAllFromMonitoringTable() {
		$this->db->select('*');
		$this->db->from('monitoring');
		$this->db->join('product', 'product.product_id = monitoring.monitoring_id');
		$this->db->join('market', 'market.market_id = monitoring.market_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function getSummarizedReport() {
		$query = $this->db->get('summary_prev_price');
		return $query->result();
	}

	public function getMonitoringEntryProd($id) {
		$this->db->where('product_id', $id);
		$query = $this->db->get('summary_prev_price');
		return $query->result();
	}
}